#!/usr/bin/env python
# coding: utf-8
# -- Author: Luiz Gustavo Barbosa Souza

import sys 
import json
import os 
import io
import commands
import re
import barcodenumber
from base import base


class json_validator(base):
    
    def __init__(self, entity):
        self.entity = entity
        super(json_validator, self).__init__()

    def __validate_pattern(self, pattern, value):
        return bool(re.match(pattern, str(value))) if value is not None else True

    def validate_json(self, entity, filename, json, id):
        try:
            for attribute in self.get_dictonary(entity):
                name_attribute = attribute.get('name')
                value = json.get(name_attribute)

                if value is not None and type(value) == unicode:
                    value = value.encode("utf-8")
                
                #campo obrigatorio nao esta presente?
                if attribute.get('required') and attribute.get('name') not in json: 
                    self.logger.error("Filename: %s - ID: %s - Atributo %s não encontrado" % (filename, id, attribute.get('name')))

                #validacao de campos do json
                if attribute.get('name') in json:
                    
                    #validacao por regex
                    if not self.__validate_pattern(str(attribute.get('pattern', "")), str(value)):
                       self.logger.error("Filename: %s - ID: %s - Formato inválido para o atributo %s." % (filename, id, attribute.get('name')))

                    #validacao tamanho minimo
                    if attribute.get('min-length') is not None:
                        if len(str(value)) < attribute.get('min-length'):
                            self.logger.error("Filename: %s - ID: %s - Os caracteres do %s não devem ser menores que %s." % (filename, id, attribute.get('name'), attribute.get('min-length')))                
                    
                    #validacao tamanho maximo
                    if attribute.get('max-length') is not None:
                        if len(str(value)) > attribute.get('max-length') and attribute.get('max-length') is not None:
                            self.logger.error("Filename: %s - ID: %s - Os caracteres do %s não devem ser maiores que %s." % (filename, id, attribute.get('name'), attribute.get('max-length')))

                    #validacao valor minimo
                    if attribute.get('min-value') is not None and (attribute.get('type') == "float" or attribute.get('type') == "int"):
                        if float(value) < attribute.get('min-value'):
                            self.logger.error("Filename: %s - ID: %s - O valor %s (%s) não pode ser inferior a %s." % (filename, id, attribute.get('name'), value, attribute.get('min-value')))                
                    

                    #validacao valor maximo
                    if attribute.get('max-value') is not None and (attribute.get('type') == "float" or attribute.get('type') == "int"):
                        if float(value) > attribute.get('max-value'):
                            self.logger.error("Filename: %s - ID: %s - O valor %s (%s) não pode ser superior a %s." % (filename, id, attribute.get('name'), value, attribute.get('max-value')))                

                    #validacao do tipo dos campos // conversao do que vem do dicionario
                    if attribute.get('type') is not None:
                        if type(value) == int and attribute.get('type') == "float":
                            value = float(value)
                        if attribute.get('type') == "float":
                            tipo = float
                        elif attribute.get('type') == "string":
                            tipo = str
                        elif attribute.get('type') == "list":
                            tipo = list
                        elif attribute.get('type') == 'int':
                            tipo = int

                    if attribute.get('type') is not None:
                        if type(value) != tipo:
                            #print str(type(value)) + " " + str(value) + " " + attribute.get('name')
                            self.logger.error("Filename: %s - ID: %s - O tipo de atributo % s deve ser %s" % (filename, id, attribute.get('name'), attribute.get('type')))
                        
                    #validacao de campos que contem enum
                    if attribute.get('enum') is not None and json.get(attribute.get('name')) not in attribute.get('enum'):
                        self.logger.error("Filename: %s - ID: %s - Atributo %s enum inválido, esperado %s." % (filename, id, attribute.get('name'), attribute.get('enum')))

                    #validacao type-value (cnpj, cpf e ean) - module brutils
                    #type_value = attribute.get('type-value')
                    #if type_value is not None:
                    #    if type_value == "cnpj" and not cnpj.validate(value):
                    #        print "Filename: %s - ID: %s - Invalid CNPJ (%s) for %s." % (filename, id, value, name_attribute)
                    #    if type_value == "cpf" and not cpf.validate(value):
                    #        print "Filename: %s - ID: %s - Invalid CPF (%s) for %s." % (filename, id, value, name_attribute)
        except Exception as e:
            self.logger.error("Filename: %s - ID: %s - Ocorreu uma excessão em %s - %s" % (filename, id, attribute.get('name'), str(e))) 
    

            
